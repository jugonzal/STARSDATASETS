# STARSDATASET

 Datasets used by the Inria STARS team

## Introduction
This repository is to keep track of the downloaded  datasets available in order to prevent duplicates.
If you need to download a new dataset, please place it under /data/stars/share/STARSDATASETS.

If you need information about the dataset and how it should be used, there is a contact next to said dataset.

Let's keep this page updated for our own benefits.

## Datasets

The datasets downloaded are unzipped ad not in their raw state, when working with them use symbolic links.


```sh
   ln -s /data/stars/share/STARSDATASETS/dataset /your/working/folder
 ```


| DATASETS		| SUBJECT OF  RESEARCH		|	LINK PAGE	|	CURRENT USERS	|
| :---	| :---: | :---: | :---: |
| 2DMOT2015		| People Tracking | [Link Page](https://motchallenge.net/results/2D_MOT_2015/) | thi-lan-anh.nguyen@inria.fr, juan-diego.gonzales-zuniga@inria.fr |
| apolloscapes	| People Detection | [Link Page]() | ujjwal.ujjwal@inria.fr |
| bd100k	| People Detection | [Link Page]() | ujjwal.ujjwal@inria.fr |
| Caltech		| Pedestrian Detection | [Link Page]() | ujjwal.ujjwal@inria.fr, abdelrahman-gaber.abubakr@inria.fr |
| catsdogs        	| Object Classification | [Link Page](https://www.kaggle.com/c/dogs-vs-cats) | ion.mosnoi@inria.fr |
| cityscapes	| Scene Understanding  | [Link Page](https://www.cityscapes-dataset.com/) | ujjwal.ujjwal@inria.fr |
| COCO	        | Object Classification | [Link Page](http://cocodataset.org/#home) | ion.mosnoi@inria.fr |
| DALY              | Action Recognition | [Link Page](http://thoth.inrialpes.fr/daly/) | yaohui.wang@inria.fr |
| DAVIS             | Object Segmentation | [Link Page](http://davischallenge.org/) | yaohui.wang@inria.fr |
| GAARD             | Action Recognition | [Link Page](https://team.inria.fr/stars/demcare-gaadrd-dataset/) | abhishek.goel@inria.fr |
| iLIDS-VID        | Person Re-ID | [Link Page](http://www.eecs.qmul.ac.uk/~xiatian/downloads_qmul_iLIDS-VID_ReID_dataset.html) | juan-diego.gonzales-zuniga@inria.fr |
| iLIDS-VID-FKhan        | Person Re-ID | [Link Page]() | juan-diego.gonzales-zuniga@inria.fr |
| ILSVRC2017 	| Imagent Visual recognition  | [Link Page](http://image-net.org/challenges/LSVRC/2017/) | ujjwal.ujjwal@inria.fr |
| JHMDB		| Human action | [Link Page](http://jhmdb.is.tue.mpg.de/dataset) | yaohui.wang@inria.fr |
| mall_dataset	| People Counting | [Link Page]() | ion.mosnoi@inria.fr |
| MARS		| Person Re-ID | [Link Page](http://www.liangzheng.com.cn/Project/project_mars.html) | juan-diego.gonzales-zuniga@inria.fr |
| MIO	| People Detection | [Link Page]() | ujjwal.ujjwal@inria.fr |
| MOT16		| People Tracking | [Link Page](https://motchallenge.net/data/MOT16/) | thi-lan-anh.nguyen@inria.fr, juan-diego.gonzales-zuniga@inria.fr |
| NTU_MagOri        | Action Recognition | [Link Page]() | carlos-antonio.caetano-junior@inria.fr |
| PASCAL3D+_release | 3D Object Reconstruction | [Link Page](http://cvgl.stanford.edu/projects/pascal3d.html) | abhishek.goel@inria.fr |
| Penn_Action       | Action Recognition  | [Link Page](https://dreamdragon.github.io/PennAction/) | kaustubh-sanjay.sakhalkar@inria.fr |
| PRID	        | Person Re-ID | [Link Page](https://www.tugraz.at/institute/icg/research/team-bischof/lrs/downloads/PRID11/) | juan-diego.gonzales-zuniga@inria.fr |
| PRID-FKHAN        | Person Re-ID | [Link Page]() | juan-diego.gonzales-zuniga@inria.fr |
| Smarthomes        | Action Recognition | [Link Page]() | rui.dai@inria.fr |
| UCF101	        | Action Recognition | [Link Page](http://crcv.ucf.edu/data/UCF101.php) | michal.koperski@inria.fr |
| UCF101_24_annotations | Action Recognition | [Link Page]() | yaohui.wang@inria.fr |
| UCF101_Depth      | Action Recognition | [Link Page]() | carlos-antonio.caetano-junior@inria.fr |
| UCF101_MagOri	| Action Recognition | [Link Page]() | carlos-antonio.caetano-junior@inria.fr |
| UCF101_MagOriOld	| Action Recognition | [Link Page]() | carlos-antonio.caetano-junior@inria.fr |
| UCLA-Northwestern | Action Recognition | [Link Page](http://users.eecs.northwestern.edu/~jwa368/my_data.html) | kaustubh-sanjay.sakhalkar@inria.fr |
| UWA3DII		| Action Recognition | [Link Page](http://staffhome.ecm.uwa.edu.au/~00053650/databases.html) | kaustubh-sanjay.sakhalkar@inria.fr |
| VOCdevkit        | Object Classification | [Link Page]() | juan-diego.gonzales-zuniga@inria.fr |
| DALIA  | Activity Recognition | [Link Page]() | abdelrahman-gaber.abubakr@inria.fr |

 
